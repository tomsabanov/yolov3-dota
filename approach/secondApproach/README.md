This directory includes the inital weights file along with two .cfg files, each having its own directory
(weights_frozen and weights_finetuned) containing weights and their corresponding map.txt files.

NOTE: The configurations in the .cfg files are most likely not the ones that generated the results .weights and map.txt files. They are probably the last configuration used, which failed to get good results.
