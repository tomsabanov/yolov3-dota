This directory includes the initial .weights files and the yolov3-tiny.cfg file.
In weights folder are gathered all of the weights and their corresponding map.txt files.

NOTE: The weights that are present in the directory weren't  necessarily gained with the yolov3-tiny.cfg file. The whole architecture is the same, but I have changed the parameters learning_rate, subdivisions and width and height throughout the training depending on the result, so the present .cfg file was used for iterations from 18,000 onwarwd.

There is also another subdirectory in the weights folder "failed_weights", which contains weight files for iterations 19,000 - 24,000, whose results were worse then from the iteration 18,0000.
